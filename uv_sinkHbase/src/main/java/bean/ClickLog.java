package bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClickLog {
    //c3 210.32.72.20 - [22/Apr/2022:10:18:53 +0800] "POST /login_form HTTP/1.1" Mozilla/5.0
     String uid;
     String Ip;
     String TrackTime ;
     String REQUESTS;
     String REQUEST_STATUS;
     String PORT;
     String REFER_DOMAIN_ARRAY;
     String USER_AGENTS;
}
