package com.utils;

import lombok.Data;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.List;
import java.util.Map;
import java.util.Set;


/*
*   操作Redis工具类
*
* */

public class RedisUtil {


   //生成Jedis线程池对象
    private static JedisPoolConfig config = new JedisPoolConfig();
    private static JedisPool jedisPool;

    public static JedisPool getJedis(){
        //是否启用后进先出, 默认true
        //config.setLifo(true)
        //最大空闲连接数, 默认8个
        config.setMaxIdle(8);
        //最大连接数, 默认8个
        config.setMaxTotal(1000);
        //获取连接时的最大等待毫秒数(如果设置为阻塞时BlockWhenExhausted),如果超时就抛异常, 小于零:阻塞不确定的时间,  默认-1
        config.setMaxWaitMillis(-1);
        //逐出连接的最小空闲时间 默认1800000毫秒(30分钟)
        config.setMinEvictableIdleTimeMillis(1800000);
        //最小空闲连接数, 默认0
        config.setMinIdle(0);
        //每次逐出检查时 逐出的最大数目 如果为负数就是 : 1/abs(n), 默认3
        config.setNumTestsPerEvictionRun(3);
        //对象空闲多久后逐出, 当空闲时间>该值 且 空闲连接>最大空闲数 时直接逐出,不再根据MinEvictableIdleTimeMillis判断  (默认逐出策略)
        config.setSoftMinEvictableIdleTimeMillis(1800000);
        //在获取连接的时候检查有效性, 默认false
        config.setTestOnBorrow(false);
        //在空闲时检查有效性, 默认false
        config.setTestWhileIdle(false);

        //初始化redis连接池对象
        jedisPool = new JedisPool(config, ConfigReader.redis_server_ip, Integer.parseInt("6379"));

        return jedisPool;
    }

    public static void main(String[] args) {

        //获取redis的连接（通过Jedis线程池创建Jedis对象）
        Jedis resource = getJedis().getResource();
        //指定维度数据所在的数据库的索引（）
        resource.select(0);
        //hash
        Long hset = resource.hset("test", "3", "this is a test too");
        System.out.println(hset);
        String testhash = resource.hget("test", "3");
        System.out.println(testhash);
        System.out.println("==========");
        System.out.println(resource.get("name"));
        System.out.println("==========");
        System.out.println(resource.hget("test","4" ));
        System.out.println("==========");
        System.out.println(resource.lset("help",2,"done"));
        System.out.println("==========");
        System.out.println(resource.get("help"));

    }

}
