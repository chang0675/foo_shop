package com.utils;

import canal.bean.RowData;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Properties;

//kafka工具类
public class KafkaUtil {
    //静态变量
    public static Properties kafkaProps;
    //静态代码块
    static {
        kafkaProps = new Properties();
        kafkaProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, ConfigReader.bootstrap_servers);
        kafkaProps.put(ConsumerConfig.GROUP_ID_CONFIG, ConfigReader.group_id);
        kafkaProps.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, ConfigReader.enable_auto_commit);
        kafkaProps.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, ConfigReader.auto_commit_interval_ms);
        kafkaProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, ConfigReader.auto_offset_reset);
        kafkaProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ConfigReader.key_deserializer);
        kafkaProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ConfigReader.value_deserializer);
    }


    //静态变量
    public static Properties kafkaCons;
    //静态代码块
    static {
        kafkaCons = new Properties();
        kafkaCons.put("bootstrap.servers", "hadoop106:9092,hadoop107:9092,hadoop108:9092");
        kafkaCons.put("acks", "all");
        kafkaCons.put("将delivery.timeout.ms","30001");
        kafkaCons.put("retries", "3");
        kafkaCons.put("batch.size", "1024");
        kafkaCons.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaCons.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaCons.put("buffer.memory", "133554432");
    }


}
