package com.utils;

import com.utils.pool.ConnectionPoolConfig;
import com.utils.pool.HbaseConnectionPool;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;

/**
 * @Description:  hbase连接池工具类
 * @Author: Sky
 * @Times : 2021/8/11 19:57
 */
public class HbaseUtil {
   public static HbaseConnectionPool getHbasePool(){
     ConnectionPoolConfig config=new ConnectionPoolConfig();
     //最大空闲连接数，默认20个
     config.setMaxIdle(20);
     //最大连接数，默认20个
     config.setMaxTotal(20);
     //获取连接时的最大等待毫秒数(如果设置为阻塞时BlockWhenExhausted),如果超时就抛异常, 小于零:阻塞不确定的时间,  默认1000
     config.setMaxWaitMillis(1000);
     //在获取连接的时候检查有效性, 默认false
     config.setTestOnBorrow(true);
     Configuration hbaseConfig = HBaseConfiguration.create();
     hbaseConfig.set("hbase.zookeeper.quorum","hadoop106,hadoop107,hadoop108");
     hbaseConfig.set("hbase.defaults.for.version.skip","true");
     //创建连接池对象
     HbaseConnectionPool pool=new HbaseConnectionPool(config,hbaseConfig);
     return  pool;



   }

}
