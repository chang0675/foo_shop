package com.utils;

import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class KafkaMySink extends RichSinkFunction<String> {


    @Override
    public void invoke(String value, Context context) throws Exception {

        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(KafkaUtil.kafkaCons);

        ProducerRecord<String, String> logs = new ProducerRecord<>("dwd_click_wide_logs", null, value);

        kafkaProducer.send(logs);

    }
}
