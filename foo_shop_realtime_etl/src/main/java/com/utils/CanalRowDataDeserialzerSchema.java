package com.utils;

import org.apache.flink.api.common.serialization.AbstractDeserializationSchema;
import canal.bean.RowData;
import java.io.IOException;

public class CanalRowDataDeserialzerSchema extends AbstractDeserializationSchema<RowData> {

    @Override
    public RowData deserialize(byte[] bytes) throws IOException {
        return new RowData(bytes);
    }
}
