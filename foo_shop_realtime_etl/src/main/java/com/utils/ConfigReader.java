package com.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 读取配置文件
 */
public class ConfigReader {
    //静态变量
    private static  Properties properties;
    //通过类加载器获取配置文件的信息
    static {
            properties=new Properties();
            InputStream inputStream = ConfigReader.class.getClassLoader().getResourceAsStream("application.properties");
            try {
                properties.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    //kafka信息
    public static String bootstrap_servers = properties.getProperty("bootstrap.servers");
    public static String zookeeper_connect = properties.getProperty("zookeeper.connect");
    public static String group_id = properties.getProperty("group.id");
    public static String enable_auto_commit = properties.getProperty("enable.auto.commit");
    public static String auto_commit_interval_ms = properties.getProperty("auto.commit.interval.ms");
    public static String auto_offset_reset = properties.getProperty("auto.offset.reset");
    public static  String key_serializer = properties.getProperty("key.serializer");
    public static  String key_deserializer = properties.getProperty("key.deserializer");
    public static  String value_deserializer = properties.getProperty("value.deserializer");
    //ip库
    public static  String ip_file_path = properties.getProperty("ip.file.path");
    //redis配置
    public static  String redis_server_ip = properties.getProperty("redis.server.ip");
    public static  String redis_server_port = properties.getProperty("redis.server.port");
    //mysql配置
    public static  String mysql_server_ip = properties.getProperty("mysql.server.ip");
    public static  String mysql_server_port = properties.getProperty("mysql.server.port");
    public static  String mysql_server_database = properties.getProperty("mysql.server.database");
    public static  String mysql_server_username = properties.getProperty("mysql.server.username");
    public static  String mysql_server_password = properties.getProperty("mysql.server.password");
    //kafka主题
    public static  String input_topic_canal = properties.getProperty("input.topic.canal");
    public static  String input_topic_click_log = properties.getProperty("input.topic.click_log");
    public static  String input_topic_cart = properties.getProperty("input.topic.cart");
    public static  String input_topic_comments = properties.getProperty("input.topic.comments");
    //Druid Kafka数据源 topic名称
    public static  String output_topic_order = properties.getProperty("output.topic.order");
    public static  String output_topic_order_detail = properties.getProperty("output.topic.order_detail");
    public static  String output_topic_cart = properties.getProperty("output.topic.cart");
    public static  String output_topic_clicklog = properties.getProperty("output.topic.clicklog");
    public static  String output_topic_goods = properties.getProperty("output.topic.goods");
    public static  String output_topic_ordertimeout = properties.getProperty("output.topic.ordertimeout");
    public static  String output_topic_comments = properties.getProperty("output.topic.comments");
    //HBase订单明细表配置
    public static  String hbase_table_orderdetail = properties.getProperty("hbase.table.orderdetail");
    public static  String hbase_table_family = properties.getProperty("hbase.table.family");



    //测试
    public static void main(String[] args) {

        System.out.println(bootstrap_servers);
        System.out.println(zookeeper_connect);
        System.out.println(group_id);
        System.out.println(enable_auto_commit);
        System.out.println(auto_commit_interval_ms);
        System.out.println(auto_offset_reset);
    }


}
