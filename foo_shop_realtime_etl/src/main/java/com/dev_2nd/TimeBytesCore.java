package com.dev_2nd;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.base.MQBaseETL;
import com.bean.ClickLogWideEntity;
import com.bean.TimeBytes;
import com.utils.KafkaUtil;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class TimeBytesCore {

    public static void main(String[] args) throws Exception {

        TimeBytesCore n = new TimeBytesCore();

        n.process();

    }

    private void process() throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        MQBaseETL baseEtl = new MQBaseETL();
        DataStream<String> clickLogDataStream = baseEtl.KafkaConsummer("dwd_click_wide_logs", env);

        DataStream<ClickLogWideEntity> clickLog = clickLogDataStream.map(new RichMapFunction<String, ClickLogWideEntity>() {
            @Override
            public ClickLogWideEntity map(String s) throws Exception {
                ClickLogWideEntity clickLogWideEntity = JSONObject.parseObject(s, ClickLogWideEntity.class);

                return clickLogWideEntity;
            }
        });

        clickLog.print();

        SingleOutputStreamOperator<Tuple2<String, Integer>> map = clickLog.map(new RichMapFunction<ClickLogWideEntity, Tuple2<String, Integer>>() {
            @Override
            public Tuple2<String, Integer> map(ClickLogWideEntity clickLogWideEntity) throws Exception {
                String province = clickLogWideEntity.getProvince();
                int i = Integer.parseInt(clickLogWideEntity.getReponseBodyBytes());

                return new Tuple2<>(province, i);
            }
        });


        map.keyBy(0)
                .timeWindow(Time.seconds(6))
                .sum(1)
//                .print();
                .addSink(new RichSinkFunction<Tuple2<String, Integer>>() {
                    @Override
                    public void invoke(Tuple2<String, Integer> value, Context context) throws Exception {

                        TimeBytes timeBytes = new TimeBytes(value.f0, value.f1);
                        String s2 = JSON.toJSONString(timeBytes, SerializerFeature.DisableCircularReferenceDetect);

                        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(KafkaUtil.kafkaCons);

                        ProducerRecord<String, String> logs = new ProducerRecord<>("time_bytes", null, s2);

                        kafkaProducer.send(logs);
                    }
                });


        env.execute("");


    }


}
