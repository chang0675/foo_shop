package com.dev_2nd;

import com.alibaba.fastjson.JSONObject;
import com.base.MQBaseETL;
import com.bean.ClickLogWideEntity;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.LocalStreamEnvironment;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;

public class BounceRate {

    public static void main(String[] args) throws Exception {

        BounceRate rate = new BounceRate();

        rate.process();

    }

    public void process() throws Exception {




    }

    public void process2() throws Exception {
        //创建流处理环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从kafka拿到点击日志
        MQBaseETL baseEtl = new MQBaseETL();
        DataStream<String> clickLogDataStream = baseEtl.KafkaConsummer("dwd_click_wide_logs", env);
        //转换为对应实体
        DataStream<ClickLogWideEntity> clickLog = clickLogDataStream.map(new RichMapFunction<String, ClickLogWideEntity>() {
            @Override
            public ClickLogWideEntity map(String s) throws Exception {
                ClickLogWideEntity clickLogWideEntity = JSONObject.parseObject(s, ClickLogWideEntity.class);

                return clickLogWideEntity;
            }
        });

        clickLog.print();

        SingleOutputStreamOperator<Tuple2<String, Integer>> map = clickLog.map(new RichMapFunction<ClickLogWideEntity, Tuple2<String, Integer>>() {
            @Override
            public Tuple2<String, Integer> map(ClickLogWideEntity clickLogWideEntity) throws Exception {
                //转换 <uid,1>
                String uid = clickLogWideEntity.getUid();
                return new Tuple2<>(uid, 1);
            }
        });


        map.keyBy(0)
                .timeWindow(Time.seconds(6))
                .sum(1)
                .print();




        env.execute();

    }

}
