package com.bean;

import lombok.Data;

/**
 * @Description:
 * @Author: Sky
 * @Times : 2021/8/14 10:36
 */

@Data
public class CommentsWideEntity {
    private String userId;        // 用户ID
    private String userName;      // 用户名
    private String orderGoodsId;  // 订单明细ID
    private Integer starScore;        // 评分
    private String comments;      // 评论
    private String assetsViedoJSON; // 图片、视频JSO
    private String createTime;     // 评论时间
    private String goodsId;      // 商品id
    private String goodsName;   //商品名称
    private Long shopId;      //商家id    ---扩宽后的字段
    private String shopName;      //商家id    ---扩宽后的字段


}
