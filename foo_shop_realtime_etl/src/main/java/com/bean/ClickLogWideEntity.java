package com.bean;

import lombok.Data;

/**
 * @Description: 定义拉宽后的点击流日志对象
 * * 因为后续需要将拉宽后的点击流对象序列化成json字符串保存到kafka集群，所以需要实现属性的set和get方法
 * @Author: Sky
 * @Times : 2021/8/13 20:16
 */
@Data
public class ClickLogWideEntity {
    private String uid; //用户id
    private String ip; //ip地址
    private String requestTime; //访问时间
    private String requestMethod;//请求方式
    private String requestUrl; //请求地址
    private String requestProtocol; //请求协议
    private String reponseStatus; //响应码
    private String reponseBodyBytes;//返回的数据流量
    private String referer; //访客来源
    private String userAgent; //用户代理信息
    private String refererDomain; //跳转过来的域名
    private String province; //拓宽后的字段：省份
    private String city;//拓宽后的字段：城市
    private String requestDateTime;//拓宽后的字段：时间戳

    public static ClickLogWideEntity getClickLogWideEntry(ClickLogEntity clickLogEntity) {
        ClickLogWideEntity clickWide = new ClickLogWideEntity();
        clickWide.setUid(clickLogEntity.getConnectionClientUser());
        clickWide.setIp(clickLogEntity.getIp());
        clickWide.setRequestTime(clickLogEntity.getRequestTime());
        clickWide.setRequestMethod(clickLogEntity.getMethod());
        clickWide.setRequestUrl(clickLogEntity.getResolution());
        clickWide.setRequestProtocol(clickLogEntity.getRequestProtocol());
        clickWide.setReponseStatus(clickLogEntity.getResponseStatus());
        clickWide.setReponseBodyBytes(clickLogEntity.getResponseBodyBytes());
        clickWide.setReferer(clickLogEntity.getReferer());
        clickWide.setUserAgent(clickLogEntity.getUserAgent());
        clickWide.setRefererDomain(clickLogEntity.getReferDomain());
        //拓宽的字段省份，城市，时间戳
        clickWide.setProvince("");
        clickWide.setCity("");
        clickWide.setRequestTime("");
        return clickWide;


    }
}
