package com.bean;

import lombok.Data;
import nl.basjes.parse.httpdlog.HttpdLoglineParser;

/**
 * @Description:
 * @Author: Sky
 * @Times : 2021/8/9 20:08
 */
@Data
public class ClickLogEntity {

    private String connectionClientUser;  //用户id信息
    private String Ip;  //IP地址
    private String requestTime;  //请求时间
    private String method;   // 请求方式
    private String resolution; //  请求资源
    private String requestProtocol; //请求协议
    private String responseStatus; //响应码
    private String responseBodyBytes; //返回的数据流量
    private String referer; //访客的来源url
    private String userAgent; //客户端代理信息
    private String referDomain; //跳转过来页面的域名:HTTP.HOST:request.referer.host

    //定义解析规则（
    public static String getLogFormat() {
        return "%u %h %l %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"";
    }



    //定义要解析的数据样本（测试）
    public static String getInputLine() {
        return "2001:980:91c0:1:8d31:a232:25e5:85d 222.68.172.190 - [05/Sep/2010:11:27:50 +0200] \"GET /images/my.jpg HTTP/1.1\" 404 23617 \"http://www.angularjs.cn/A00n\" \"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; nl-nl) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8\"";
    }


    //根据定义的日志规则解析数据（测试）
    public static HttpdLoglineParser<ClickLogEntity> createClickLogParse() throws Exception {
        HttpdLoglineParser<ClickLogEntity> parser = new HttpdLoglineParser<>(ClickLogEntity.class, getLogFormat());
        parser.addParseTarget("setConnectionClientUser", "STRING:connection.client.user");
        parser.addParseTarget("setIp", "IP:connection.client.host");
        parser.addParseTarget("setRequestTime", "TIME.STAMP:request.receive.time.last");
        parser.addParseTarget("setMethod", "HTTP.METHOD:request.firstline.method");
        parser.addParseTarget("setResolution", "HTTP.URI:request.firstline.uri");
        parser.addParseTarget("setRequestProtocol", "HTTP.PROTOCOL:request.firstline.protocol");
        parser.addParseTarget("setResponseStatus", "STRING:request.status.last");
        parser.addParseTarget("setResponseBodyBytes", "BYTES:response.body.bytes");
        parser.addParseTarget("setReferer", "HTTP.URI:request.referer");
        parser.addParseTarget("setUserAgent", "HTTP.USERAGENT:request.user-agent");
        parser.addParseTarget("setReferDomain", "HTTP.HOST:request.referer.host");
        ClickLogEntity record = new ClickLogEntity();
//        根据定义的解析规则解析样本数据
        parser.parse(record, getInputLine());

        System.out.println(record.getConnectionClientUser());
        System.out.println(record.getIp());
        System.out.println(record.getRequestTime());
        System.out.println(record.getMethod());
        System.out.println(record.getResolution());
        System.out.println(record.getRequestProtocol());
        System.out.println(record.getResponseStatus());
        System.out.println(record.getResponseBodyBytes());
        System.out.println(record.getReferer());
        System.out.println(record.getUserAgent());
        System.out.println(record.getReferDomain());

        return parser;
    }



//    public static ClickLogEntity clickLogEntity(String clickLog) throws Exception {
//        ClickLogEntity clickLogEntity = new ClickLogEntity();
//        return praser.parse(clickLogEntity, clickLog);
//    }

    public static void main(String[] args) throws Exception {

        ClickLogEntity.createClickLogParse();

    }


}
