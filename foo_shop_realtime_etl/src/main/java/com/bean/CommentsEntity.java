package com.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;


/**
 * @Description:
 * @Author: Sky
 * @Times : 2021/8/10 11:38
 */
@Data
public class CommentsEntity {

    private String userId;    // 用户ID
    private String userName;  // 用户名
    private String orderGoodsId;// 订单明细ID
    private Integer starScore;    // 评分
    private String comments;  // 评论
    private String assetsViedoJSON; // 图片、视频JSO
    private String goodsId; //商品id
    private Long timestamp; // 评论时间

    //将传进来的字符串转换为评论对象
    public static CommentsEntity getCommentsEntity(String json) {
        JSONObject jsonObject = JSON.parseObject(json);
        CommentsEntity commentsEntity = new CommentsEntity();
        commentsEntity.setUserId(jsonObject.getString("userId"));
        commentsEntity.setUserName(jsonObject.getString("userName"));
        commentsEntity.setGoodsId(jsonObject.getString("orderGoodsId"));
        commentsEntity.setStarScore(jsonObject.getInteger("starScore"));
        commentsEntity.setComments(jsonObject.getString("comments"));
        commentsEntity.setAssetsViedoJSON(jsonObject.getString("assetsViedoJSON"));
        commentsEntity.setGoodsId(jsonObject.getString("goodsId"));
        commentsEntity.setTimestamp(jsonObject.getLong("timestamp"));
        return commentsEntity;


    }


}
