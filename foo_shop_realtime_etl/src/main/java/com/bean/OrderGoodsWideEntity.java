package com.bean;

import lombok.Data;

/**
 * @Description: 订单明细拉宽数据
 * @Author: Sky
 * @Times : 2021/8/10 16:35
 */

@Data
public class OrderGoodsWideEntity {
    private Long orgId;               //订单明细id
    private Long orderId;            //订单id
    private Long goodsId;            //商品id
    private Long goodsNum;            //商品数量
    private Double goodsPrice;       //商品价格
    private String goodsName;         //商品名称
    private Long shopId;             //拓宽后的字段-》商品表：店铺id
    private Integer goodsThirdCatId;      //拓宽后的字段->商品表：商品三级分类id
    private String goodsThirdCatName; //拓宽后的字段->商品分类表：商品三级分类名称
    private Integer goodsSecondCatId;     //拓宽后的字段-》商品分类表：商品二级分类id
    private String goodsSecondCatName;  //拓宽后的字段-》商品分类表：商品二级分类名称
    private Integer goodsFirstCatId;        //拓宽后的字段-》商品分类表：商品一级分类id
    private String goodsFirstCatName;  //拓宽后的字段-》商品分类表：商品一级分类名称
    private Integer areaId;                //拓宽后的字段-》店铺表：区域id
    private String shopName;          //拓宽后的字段-》店铺表：店铺名称
    private String shopCompany;        //拓宽后的字段-》店铺表：店铺所属公司
    private Integer cityId;                 //拓宽后的字段-》组织机构表：城市id
    private String cityName;           //拓宽后的字段-》组织机构表：城市名称
    private Integer regionId;            //拓宽后的字段-》组织机构表：大区id
    private String regionName;          //拓宽后的字段-》组织机构表：大区名称
    private String orgName;             //拓宽后的字段-》组织机构表：组织名称


}
