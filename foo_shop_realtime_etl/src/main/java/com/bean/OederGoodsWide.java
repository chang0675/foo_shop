package com.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OederGoodsWide {

    private Long ogId;               //订单详情ID
    private Long orderId;           //订单ID
    private Long goodsId;           //商品ID
    private Long goodsNum;          //商品数量
    private Double goodsPrice;      //价格
    private String goodsName;      //商品名称
    private Long shopId;            //拉宽字段--店铺ID
    private Integer goodsThirdCatId;    //拉宽字段--三级ID
    private String goodsThirdCatName; //拉宽字段--三级名称
    private Integer goodsSecondCatId;    //拉宽字段--二级id
    private String goodsSecondCatName;//拉宽字段--二级名称
    private Integer goodsFirstCatId;     //拉宽字段--一级ID
    private String goodsFirstCatName;//拉宽字段--一级名称
    private Integer areaId;               //拉宽字段--地区ID
    private String shopName;          //拉宽字段--店铺名称
    private String shopCompany;       //拉宽字段--店铺公司
    private Integer cityId;              //拉宽字段--城市ID
    private String cityName;         //拉宽字段--城市名
    private Integer regionId;             //拉宽字段--组织ID
    private String regionName;       //拉宽字段--组织机构

}
