package com.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
// 组织结构维度实体类    itcast_org
public class DimOrgDBEntity {
    private Integer orgId;// 机构id
    private Integer parentId;// 机构父id
    private String orgName;// 组织机构名称
    private Integer orgLevel;// 组织机构级别

    public static DimOrgDBEntity getDimOrg(String json){
        if(!(StringUtils.isBlank(json))){
            JSONObject jsonObject= JSON.parseObject(json);
            DimOrgDBEntity dimOrg=new DimOrgDBEntity();
            dimOrg.setOrgId(jsonObject.getInteger("orgId"));
            dimOrg.setOrgLevel(jsonObject.getInteger("orgLevel"));
            dimOrg.setOrgName(jsonObject.getString("orgName"));
            dimOrg.setParentId(jsonObject.getInteger("parentId"));
            return  dimOrg;
        }
        return null;
    }
}
