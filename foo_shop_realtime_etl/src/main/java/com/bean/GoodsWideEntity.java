package com.bean;

import lombok.Data;

/**
 * @Description: 商品样例类（宽表）
 * @Author: Sky
 * @Times : 2021/8/10 15:02
 */

@Data
public class GoodsWideEntity {
    private Long goodsId;          //商品id
    private String goodsSn;         //商品编号
    private String productNo;       //商品货号
    private String goodsName;       //商品名称
    private String goodsImg;        //商品图片
    private String shopId;         //门店ID
    private String shopName;        //门店名称      -》拉宽的字段
    private String goodsType;     //货物类型
    private String marketPrice;     //市场价
    private String shopPrice;       //门店价
    private String warnStock;       //预警库存
    private String goodsStock;     //商品总库存
    private String goodsUnit;       //单位
    private String goodsTips;      //促销信息
    private String isSale;       //是否上架	0:不上架 1:上架
    private String isBest;         //是否精品	0:否 1:是
    private String isHot;           //是否热销产品	0:否 1:是
    private String isNew;           //是否新品	0:否 1:是
    private String isRecom;         //是否推荐	0:否 1:是
    private String goodsCatIdPath;  //商品分类ID路径	catId1_catId2_catId3
    private String goodsThirdCatId;       //三级商品分类ID    -》拉宽的字段
    private String goodsThirdCatName;  //三级商品分类名称  -》拉宽的字段
    private Integer goodsSecondCatId;      //二级商品分类ID    -》拉宽的字段
    private String goodsSecondCatName;  //二级商品分类名称  -》拉宽的字段
    private Integer goodsFirstCatId;        //一级商品分类ID    -》拉宽的字段
    private String goodsFirstCatName;   //一级商品分类名称  -》拉宽的字段
    private String catId;                //分类ID
    private String catName;              //分类名称
    private String shopCatId1;          //门店商品分类第一级ID
    private String shopCatName1;        //门店商品分类第一级名称 -》拉宽的字段
    private String shopCatId2;          //门店商品第二级分类ID
    private String shopCatName2;       //门店商品第二级分类名称 -》拉宽的字段
    private String brandId;         //品牌ID
    private String goodsDesc;      //商品描述
    private String goodsStatus;     //商品状态	-1:违规 0:未审核 1:已审核
    private String saleNum;        //总销售量
    private String saleTime;       //上架时间
    private String visitNum;        //访问数
    private String appraiseNum;    //评价书
    private String isSpec;          //是否有规格	0:没有 1:有
    private String gallery;       //商品相册
    private String goodsSeoKeywords;  //商品SEO关键字
    private String illegalRemarks;  //状态说明	一般用于说明拒绝原因
    private String dataFlag;      //	删除标志	-1:删除 1:有效
    private String createTime;
    private String isFreeShipping;
    private String goodsSerachKeywords;
    private String modifyTime;     //修改时间
    private Integer cityId;   //城市ID
    private String cityName;   //城市姓名
    private Integer regionId;   //区域ID
    private String regionName;  //地域名
}
