package com.bean;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
//商品分类维度实体类   itcast_goods_cats
public class DimGoodsCatDBEntity {
    private String catId;// 商品分类id
    private String parentId;// 商品分类父id
    private String catName;// 商品分类名称
    private String cat_level;// 商品分类级别

    //商品维度表
    public static DimGoodsCatDBEntity getDimGoodCat(String json){
        JSONObject jsonObject= JSON.parseObject(json);
        DimGoodsCatDBEntity dimGoodsCat=new DimGoodsCatDBEntity();
        if(!(StringUtils.isBlank(json))){
            dimGoodsCat.setCatId(jsonObject.getString("catId"));
            dimGoodsCat.setParentId(jsonObject.getString("parentId"));
            dimGoodsCat.setCatName(jsonObject.getString("catName"));
            dimGoodsCat.setCat_level(jsonObject.getString("cat_level"));
            return  dimGoodsCat;
        }
        return  null;
    }

}
