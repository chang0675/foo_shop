package com.bean;

import com.utils.DateStyle;
import com.utils.DateUtil;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description:  购物车样例类(拉宽)
 * @Author: Sky
 * @Times : 2021/8/9 19:57
 */
@Data
public class CartWideEntity {
  private String goodsId; // 商品ID
  private String userId;  //用户ID
  private Integer count;  //商品数量
  private String  guid;  //用户唯一标识
  private String addTime; //添加购物车时间
  private String ip;  //ip地址
  private Double goodsPrice;  //商品价格
  private String goodsName;  //商品名称
  private String goodsCat3;//商品三级分类
  private String goodsCat2; //商品二级分类
  private String goodsCat1; //商品一级分类
  private String shopId; //门店id
  private String shopName; //门店名称
  private String shopProvinceId; //门店所在省份id
  private String shopProvinceName; //门店所在省份名称
  private String shopCityId;//门店所在城市id
  private String shopCityName; //门店所在城市名称
  private String clientProvince; //客户所在省份
  private String clientCity; // 客户所在城市

  //拉宽表
  public static CartWideEntity getCartWideEntity(CartEntity cartEntity){
    CartWideEntity cartWideEntity=new CartWideEntity();
    cartWideEntity.setGoodsId(cartEntity.getGoodsId());
    cartWideEntity.setUserId(cartEntity.getUserId());
    cartWideEntity.setCount(cartEntity.getCount());
    cartWideEntity.setGuid(cartEntity.getGuid());
    String addTime = cartEntity.getAddTime();
    Long aLong = Long.valueOf(addTime);
    Date date = new Date(aLong);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String format = simpleDateFormat.format(date);
    cartWideEntity.setAddTime(format);
    cartWideEntity.setIp(cartEntity.getIp());
    cartWideEntity.setGoodsPrice(0.0);
    cartWideEntity.setGoodsName("");
    cartWideEntity.setGoodsCat1("");
    cartWideEntity.setGoodsCat2("");
    cartWideEntity.setGoodsCat3("");
    cartWideEntity.setShopId("");
    cartWideEntity.setShopName("");
    cartWideEntity.setShopProvinceId("");
    cartWideEntity.setShopProvinceName("");
    cartWideEntity.setShopCityId("");
    cartWideEntity.setShopCityName("");
    cartWideEntity.setClientProvince("");
    cartWideEntity.setClientCity("");

    return cartWideEntity;









  }


}
