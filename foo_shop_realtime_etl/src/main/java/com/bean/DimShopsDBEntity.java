package com.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
// 店铺维度样例类   itcast_shops
public class DimShopsDBEntity {
    private Integer shopId;// 店铺id
    private Integer areaId;// 店铺所属区域id
    private String shopName;// 店铺名称
    private String shopCompany;// 公司名称

    public static DimShopsDBEntity getDimShops(String json){
        if(!(StringUtils.isBlank(json))){
            JSONObject jsonObject= JSON.parseObject(json);
            DimShopsDBEntity dimShops=new DimShopsDBEntity();
            dimShops.setAreaId(jsonObject.getInteger("areaId"));
            dimShops.setShopCompany(jsonObject.getString("shopCompany"));
            dimShops.setShopId(jsonObject.getInteger("shopId"));
            dimShops.setShopName(jsonObject.getString("shopName"));
            return  dimShops;
        }

        return  null;
    }
}
