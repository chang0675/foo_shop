package com.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;


/**
 * @Description:  维度表样例类
 * @Author: Sky
 * @Times : 2021/8/10 11:57
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DimGoodsDBEntity {

    private  Long    goodsId;  //商品ID
    private  String  goodsName; //商品名称
    private  Long  shopId;  //店铺ID
    private  Integer goodsCatId ; //商品分类ID
    private  Double shopPrice;//商品价格

   //商品维度分析
   public static DimGoodsDBEntity getGoodInfo(String json) {
       if (!(StringUtils.isBlank(json))) {
           JSONObject jsonObject = JSON.parseObject(json);
           DimGoodsDBEntity goodsDBEntity = new DimGoodsDBEntity();
           goodsDBEntity.setGoodsId(jsonObject.getLong("goodsId"));
           goodsDBEntity.setGoodsName(jsonObject.getString("goodsName"));
           goodsDBEntity.setShopId(jsonObject.getLong("shopId"));
           goodsDBEntity.setGoodsCatId(jsonObject.getInteger("goodsCatId"));
           goodsDBEntity.setShopPrice(jsonObject.getDouble("shopPrice"));
           return goodsDBEntity;
       }
       return null;

   }




}
