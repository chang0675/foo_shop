package com.bean;

import canal.bean.RowData;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description: 创建订单的样例类
 * @Author: Sky
 * @Times : 2021/8/10 15:19
 */
@Data
public class OrderDBEntity {
    private Long orderId;          //订单id
    private String orderNo;        //订单编号
    private Long userId;           //用户id
    private Integer orderStatus;      //订单状态 -3:用户拒收-2:未付款的订单-1：用户取消 0:待发货 1:配送中 2:用户确认收货
    private Double goodsMoney;     //商品金额
    private Integer deliverType;       //收货方式0:送货上门1:自提
    private Double deliverMoney;  //运费
    private BigDecimal totalMoney;    //订单金额（包括运费）
    private BigDecimal realTotalMoney; //实际订单金额（折扣后金额）
    private Integer payType;           //支付方式
    private Integer isPay;           //是否支付0:未支付1:已支付
    private Integer areaId;           //区域最低一级
    private String areaIdPath;     //区域idpath
    private String userName;       //收件人姓名
    private String userAddress;    //收件人地址
    private String userPhone;      //收件人电话
    private Integer orderScore;      //订单所得积分
    private Integer isInvoice;       //是否开发票1:需要0:不需要
    private String invoiceClient;  //发票抬头
    private String orderRemarks;   //订单备注
    private Integer orderSrc;          //订单来源0:商城1:微信2:手机版3:安卓App4:苹果App
    private Double needPay;      //需缴费用
    private Integer payRand;           //货币单位
    private Integer orderType;        //订单类型
    private Integer isRefund;        //是否退款0:否1：是
    private Integer isAppraise;        //是否点评0:未点评1:已点评
    private Integer cancelReason;   //取消原因ID
    private Integer rejectReason;      //用户拒绝原因ID
    private String rejectOtherReason; //用户拒绝其他原因
    private Integer isClosed;         //订单是否关闭
    private String goodsSearchKeys;
    private String orderunique;   //订单流水号
    private String receiveTime;    //收货时间
    private String deliveryTime;  //发货时间
    private String tradeNo;       //在线支付交易流水
    private Integer dataFlag;        //订单有效标志 -1：删除 1:有效
    private String createTime;     //下单时间
    private Integer settlementId;      //是否结算，大于0的话则是结算ID
    private Double commissionFee; //订单应收佣金
    private Double scoreMoney;     //积分抵扣金额
    private Integer useScore;          //花费积分
    private String orderCode;
    private String extraJson;      //额外信息
    private Integer orderCodeTargetId;
    private Integer noticeDeliver;     //提醒发货 0:未提醒 1:已提醒
    private String invoiceJson;    //发票信息
    private Double lockCashMoney;  //锁定提现金额
    private String payTime;        //支付时间
    private Integer isBatch;           //是否拼单
    private BigDecimal totalPayFee;       //总支付金额
    private String isFromCart;  //是否来自购物车 0：直接下单  1：购物车

    //创建订单对象
    public static OrderDBEntity getOrderDB(RowData rowData) {
        OrderDBEntity order = new OrderDBEntity();
        order.setOrderId(Long.parseLong(rowData.getColumns().get("orderId")));
        order.setOrderNo(rowData.getColumns().get("orderNo"));
        order.setUserId(Long.parseLong(rowData.getColumns().get("userId")));
        order.setOrderStatus(Integer.valueOf(rowData.getColumns().get("orderStatus")));
        order.setGoodsMoney(Double.valueOf(rowData.getColumns().get("goodsMoney")));
        order.setDeliverType(Integer.valueOf(rowData.getColumns().get("deliverType")));
        order.setDeliverMoney(Double.valueOf(rowData.getColumns().get("deliverMoney")));
        order.setTotalMoney(new BigDecimal(rowData.getColumns().get("totalMoney")));
        order.setRealTotalMoney(new BigDecimal(rowData.getColumns().get("realTotalMoney")));
        order.setPayType(Integer.parseInt(rowData.getColumns().get("payType")));
        order.setIsPay(Integer.parseInt(rowData.getColumns().get("isPay")));
        order.setAreaId(Integer.parseInt(rowData.getColumns().get("areaId")));
        order.setAreaIdPath(rowData.getColumns().get("areaIdPath"));
        order.setUserName(rowData.getColumns().get("userName"));
        order.setUserAddress(rowData.getColumns().get("userAddress"));
        order.setUserPhone(rowData.getColumns().get("userPhone"));
        order.setOrderScore(Integer.parseInt(rowData.getColumns().get("orderScore")));
        order.setIsInvoice(Integer.parseInt(rowData.getColumns().get("isInvoice")));
        order.setInvoiceClient(rowData.getColumns().get("invoiceClient"));
        order.setOrderRemarks(rowData.getColumns().get("orderRemarks"));
        order.setOrderSrc(Integer.parseInt(rowData.getColumns().get("orderSrc")));
        order.setNeedPay(Double.parseDouble(rowData.getColumns().get("needPay")));
        order.setPayRand(Integer.parseInt(rowData.getColumns().get("payRand")));
        order.setOrderType(Integer.parseInt(rowData.getColumns().get("orderType")));
        order.setIsRefund(Integer.parseInt(rowData.getColumns().get("isRefund")));
        order.setIsAppraise(Integer.parseInt(rowData.getColumns().get("isAppraise")));
        order.setCancelReason(Integer.parseInt(rowData.getColumns().get("cancelReason")));
        order.setRejectReason(Integer.parseInt(rowData.getColumns().get("rejectReason")));
        order.setRejectOtherReason(rowData.getColumns().get("rejectOtherReason"));
        order.setIsClosed(Integer.parseInt(rowData.getColumns().get("isClosed")));
        order.setGoodsSearchKeys(rowData.getColumns().get("goodsSearchKeys"));
        order.setOrderunique(rowData.getColumns().get("orderunique"));
        order.setReceiveTime(rowData.getColumns().get("receiveTime"));
        order.setDeliveryTime(rowData.getColumns().get("deliveryTime"));
        order.setTradeNo(rowData.getColumns().get("tradeNo"));
        order.setDataFlag(Integer.parseInt(rowData.getColumns().get("dataFlag")));
        order.setCreateTime(rowData.getColumns().get("createTime"));
        order.setSettlementId(Integer.parseInt(rowData.getColumns().get("settlementId")));
        order.setCommissionFee(Double.valueOf(rowData.getColumns().get("commissionFee")));
        order.setScoreMoney(Double.valueOf(rowData.getColumns().get("scoreMoney")));
        order.setUseScore(Integer.parseInt(rowData.getColumns().get("useScore")));
        order.setOrderCode(rowData.getColumns().get("orderCode"));
        order.setExtraJson(rowData.getColumns().get("extraJson"));
        order.setOrderCodeTargetId(Integer.parseInt(rowData.getColumns().get("orderCodeTargetId")));
        order.setNoticeDeliver(Integer.parseInt(rowData.getColumns().get("noticeDeliver")));
        order.setInvoiceJson(rowData.getColumns().get("invoiceJson"));
        order.setLockCashMoney(Double.valueOf(rowData.getColumns().get("lockCashMoney")));
        order.setPayTime(rowData.getColumns().get("payTime"));
        order.setIsBatch(Integer.parseInt(rowData.getColumns().get("isBatch")));
        order.setTotalPayFee(new BigDecimal(Double.parseDouble(rowData.getColumns().get("totalPayFee"))));
        order.setIsFromCart(rowData.getColumns().get("isFromCart"));

        return order;


    }

}
