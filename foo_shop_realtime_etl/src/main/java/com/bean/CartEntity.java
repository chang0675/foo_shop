package com.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * @Description:  购物车样例类
 * @Author: Sky
 * @Times : 2021/8/10 10:54
 */
@Data
public class CartEntity {

  private String goodsId; //商品ID
  private String userId;  //用户ID
  private Integer count;  //商品数量
  private String  guid;  //用户唯一标识
  private String addTime; //添加购物车时间
  private String  ip ;   //访问IP地址

//将获取的值转换为JSON格式

  public static CartEntity getJsonString(String json){
    JSONObject  jsonObject=JSON.parseObject(json);
    CartEntity cartEntity=new CartEntity();
    cartEntity.setGoodsId(jsonObject.getString("goodsId"));
    cartEntity.setUserId(jsonObject.getString("userId"));
    cartEntity.setCount(jsonObject.getInteger("count"));
    cartEntity.setGuid(jsonObject.getString("guid"));
    cartEntity.setAddTime(jsonObject.getString("addTime"));
    cartEntity.setIp(jsonObject.getString("ip"));
    return  cartEntity;

  }


}
