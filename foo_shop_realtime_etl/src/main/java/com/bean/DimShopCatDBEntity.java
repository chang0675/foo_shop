package com.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
// 门店商品分类维度实体类    itcast_shop_cats
public class DimShopCatDBEntity {
    private String catId;// 商品分类id
    private String parentId;// 商品分类父id
    private String catName;// 商品分类名称
    private String catSort;// 商品分类级别

    public static DimShopCatDBEntity getDimShop(String json){
        if(!(StringUtils.isBlank(json))) {
            JSONObject jsonObject = JSON.parseObject(json);
            DimShopCatDBEntity shopCat = new DimShopCatDBEntity();
            shopCat.setCatId(jsonObject.getString("catId"));
            shopCat.setCatName(jsonObject.getString("catName"));
            shopCat.setCatSort(jsonObject.getString("catSort"));
            shopCat.setParentId(jsonObject.getString("parentId"));
            return  shopCat;

        }
        return null;
    }
}
