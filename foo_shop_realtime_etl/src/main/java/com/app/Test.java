package com.app;

import canal.bean.RowData;
import canal.util.ip.IPSeeker;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.base.MySqlBaseETL;
import com.bean.CartEntity;
import com.bean.ClickLogEntity;
import com.bean.GoodsWideEntity;
import com.bean.TimeBytes;
import com.utils.*;
import com.utils.pool.HbaseConnectionPool;
import nl.basjes.parse.httpdlog.HttpdLoglineParser;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import redis.clients.jedis.Jedis;
import java.io.IOException;
import java.util.List;


public class Test extends MySqlBaseETL{

    @org.junit.Test
    public void test11(){

        Tuple2<String, Integer> tuple2 = new Tuple2<>("广东", 22544);
        String s1 = JSON.toJSONString(tuple2, SerializerFeature.DisableCircularReferenceDetect);
        TimeBytes bytes = new TimeBytes("广东", 22544);
        String s2 = JSON.toJSONString(bytes, SerializerFeature.DisableCircularReferenceDetect);
        System.out.println(s1);
        System.out.println(s2);

    }

    @org.junit.Test
    public void test10() throws IOException {

        HbaseConnectionPool pool = HbaseUtil.getHbasePool();

        Connection conn = pool.getConnection();

        Table detail = conn.getTable(TableName.valueOf("dwd_order_detail"));

        Scan scan = new Scan();
        ResultScanner scanner = detail.getScanner(scan);

        for (Result result : scanner) {
            List<Cell> cells = result.listCells();
            for (Cell cell : cells) {
                System.out.println(Bytes.toString(CellUtil.cloneRow(cell))+"=");
            }
        }

    }

    @org.junit.Test
    public void test09() throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);
        MySqlBaseETL mysqlEtl = new MySqlBaseETL();
        //1.只过滤出来 foo_goods 表的日志，并进行转换
        DataStream<RowData> dwdGoods = mysqlEtl.KafkaConsummer("ods_shop", env);
        DataStream<RowData> goodsCanalDS = dwdGoods.filter(new FilterFunction<RowData>() {
            @Override
            public boolean filter(RowData canalRowData) throws Exception {
                return canalRowData.getTableName().equals("itcast_goods");
            }
        });
        goodsCanalDS.print();

        SingleOutputStreamOperator<GoodsWideEntity> map = goodsCanalDS.map(new RichMapFunction<RowData, GoodsWideEntity>() {

            private Jedis jedis;
            @Override
            public void open(Configuration parameters) throws Exception {
                super.open(parameters);
            }

            @Override
            public GoodsWideEntity map(RowData rowData) throws Exception {
                return null;
            }
        });

        Jedis jedis = RedisUtil.getJedis().getResource();

        jedis.select(1);


        env.execute();

    }

    @org.junit.Test
    public void test08(){

        Jedis resource = RedisUtil.getJedis().getResource();

        resource.select(0);

        String level = resource.get("c1::findSchool");

        System.out.println(level);

//        String goodsJson = resource.hget("foo_shop:dim_goods", "113136");
//        System.out.println(goodsJson);
//        DimGoodsDBEntity dimGoods = DimGoodsDBEntity.getGoodInfo(goodsJson);
//
//        String shopJson = resource.hget("foo_shop:dim_shops", dimGoods.getShopId().toString());
//        DimShopsDBEntity dimShops = DimShopsDBEntity.getDimShops(shopJson);
//        System.out.println(shopJson);
//
//        System.out.println(dimShops);


    }

    @org.junit.Test
    public void test07(){

        CartEntity entity = new CartEntity();
        entity.setIp("1");
        entity.setAddTime("asdasdas");
        entity.setCount(2);
        entity.setGoodsId("asdasd");
        entity.setGuid("eeee");
        entity.setUserId("asdsa");


        String string = JSON.toJSONString(entity);

        System.out.println(string);

    }

    @org.junit.Test
    public void test03(){

        ClickLogEntity entity = new ClickLogEntity();
        String string = DateUtil.StringToString(("05/Sep/2010:11:27:50 +0200"), DateStyle.YYYY_MM_DD_HH_MM_SS);

        System.out.println(string);


    }


    @org.junit.Test
    public void test01() throws Exception {


        IPSeeker file = new IPSeeker("qqwry.dat", "File");

        String country = file.getCountry("123.233.132.70");

        System.out.println(country);

    }

    @org.junit.Test
    public void test04() throws Exception {


        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        DataStream<String> aaa = env.fromElements("aaa");

        aaa.addSink(new KafkaMySink());

        env.execute("test");
    }


    @org.junit.Test
    public void test05() throws Exception {

        String str = "%u %h %l %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"";

        HttpdLoglineParser<ClickLogEntity> parser = new HttpdLoglineParser<>(ClickLogEntity.class, str);
        parser.addParseTarget("setConnectionClientUser", "STRING:connection.client.user");
        parser.addParseTarget("setIp", "IP:connection.client.host");
        parser.addParseTarget("setRequestTime", "TIME.STAMP:request.receive.time.last");
        parser.addParseTarget("setMethod", "HTTP.METHOD:request.firstline.method");
        parser.addParseTarget("setResolution", "HTTP.URI:request.firstline.uri");
        parser.addParseTarget("setRequestProtocol", "HTTP.PROTOCOL:request.firstline.protocol");
        parser.addParseTarget("setResponseStatus", "STRING:request.status.last");
        parser.addParseTarget("setResponseBodyBytes", "BYTES:response.body.bytes");
        parser.addParseTarget("setReferer", "HTTP.URI:request.referer");
        parser.addParseTarget("setUserAgent", "HTTP.USERAGENT:request.user-agent");
        parser.addParseTarget("setReferDomain", "HTTP.HOST:request.referer.host");

        String b = "2001:980:91c0:1:8d31:a232:25e5:85d 222.68.172.190 - [05/Sep/2010:11:27:50 +0200] \\\"GET /images/my.jpg HTTP/1.1\\\" 404 23617 \\\"http://www.angularjs.cn/A00n\\\" \\\"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; nl-nl) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8\"";

        String a ="a2bd0567-1648-45ec-b4e9-7da821827fb5 139.206.183.33 - [27/Aug/2021:03:24:32 +0800] \"GET /login_form HTTP/1.1\" 502 914 \"http://www.search.cn\" \"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; nl-nl) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8\"";

        ClickLogEntity record = new ClickLogEntity();

        ClickLogEntity parse = parser.parse(record, a);

        ClickLogEntity parse1 = parser.parse(a);
        System.out.println(record.getConnectionClientUser());
        System.out.println(record.getIp());
        System.out.println(record.getRequestTime());
        System.out.println(record.getMethod());
        System.out.println(record.getResolution());
        System.out.println(record.getRequestProtocol());
        System.out.println(record.getResponseStatus());
        System.out.println(record.getResponseBodyBytes());
        System.out.println(record.getReferer());
        System.out.println(record.getUserAgent());
        System.out.println(record.getReferDomain());
        System.out.println("---------------------------------");

        System.out.println(parse1);
        System.out.println("===================");
        System.out.println(parse);
    }

//    private static StreamExecutionEnvironment env;



//    @Before
//    public void before(){
//
//
//        env.setParallelism(1);
//        System.out.println("===启动===");
//    }

//    @org.junit.Test
//    public void test02() throws Exception {
//
//        ClickLogDataETL etl = new ClickLogDataETL();
//
//        etl.process(env);
//
//    }
//
//    @After
//    public void after() throws Exception {
//
//        env.execute("upup");
//        System.out.println("===提交===");
//    }

}
