package com.app;

import com.process.SyncDimData;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
//程序入口
public class ApplicationMain {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //异步的方式将维度数据同步到Redis中
        SyncDimData syncDimData = new SyncDimData();
        syncDimData.process(env);
        env.execute();


    }
}
