package com.base;

import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.sql.SQLException;

public interface BaseETL<T> {

   public DataStream<T> KafkaConsummer(String topic, StreamExecutionEnvironment env) throws Exception;

}
