package canal.test;

import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

public class TestDemo {


    @Test
    public void test01(){
        String ip = "43.82.154.255";
        StringTokenizer tokenizer = new StringTokenizer(ip,".");
        int anInt = Integer.parseInt(tokenizer.nextToken());
        int anInt1 = Integer.parseInt(tokenizer.nextToken());
        System.out.println(anInt);
        System.out.println(anInt1);
    }

    @Test
    public void test02(){

        long l = 1629789769000l;

        Date d = new Date(l);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:sss");

        String f = format.format(d);

        System.out.println(f);

    }
}
