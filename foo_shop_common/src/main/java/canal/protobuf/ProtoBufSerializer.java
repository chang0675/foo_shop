package canal.protobuf;

import org.apache.kafka.common.serialization.Serializer;
import java.util.Map;

//protobuf的序列化类
public class ProtoBufSerializer implements Serializer<ProtoBufable> {

    public void configure(Map<String, ?> map, boolean b) { }

    public byte[] serialize(String s, ProtoBufable protoBufable) {
        return protoBufable.toByte();
    }

    public void close() { }

}
