package canal.bean;

import canal.protobuf.ProtoBufable;
import com.alibaba.fastjson.JSON;
import com.google.protobuf.InvalidProtocolBufferException;
import lombok.Data;
import lombok.NoArgsConstructor;
import proto.CanalModel;
import java.util.HashMap;
import java.util.Map;
import static proto.CanalModel.RowData.parseFrom;


@Data
@NoArgsConstructor
public class RowData implements ProtoBufable {

    private String logfilename;//binlog 数据库的操作日志
    private Long logfileoffset;//binlog 偏移量
    private Long executeTime;//操作时间
    private String schemaName;//数据库名
    private String tableName;//表名
    private String eventType;//操作类型
    private Map<String, String> columns;//列信息

    public RowData(Map map) {
        if(map!=null){
            this.logfilename = map.get("logfileName").toString();
            this.logfileoffset = Long.parseLong(map.get("logfileOffset").toString());
            this.executeTime = Long.parseLong(map.get("executeTime").toString());
            this.schemaName = map.get("schemaName").toString();
            this.tableName = map.get("tableName").toString();
            this.eventType = map.get("eventType").toString();
            this.columns = (Map<String, String>)map.get("columns");
        }

    }

    public RowData(String logfilename,
                   Long logfileoffset,
                   Long executeTime,
                   String schemaName,
                   String tableName,
                   String eventType,
                   Map<String, String> columns) {
        this.logfilename = logfilename;
        this.logfileoffset = logfileoffset;
        this.executeTime = executeTime;
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.eventType = eventType;
        this.columns = columns;
    }

    //反序列化构造
    public RowData(byte[] bytes) {
        try {
            CanalModel.RowData rowData = parseFrom(bytes); //parseFrom是canalModle中的静态方法
            this.logfilename = rowData.getLogfileName();
            this.logfileoffset = rowData.getLogfileOffset();
            this.executeTime = rowData.getExecuteTime();
            this.tableName = rowData.getTableName();
            this.eventType = rowData.getEventType();
            // 将所有map列值添加到可变HashMap中
            this.columns = new HashMap<String, String>();
            columns.putAll(rowData.getColumnsMap());
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
    }

    //序列化方法
    public byte[] toByte() {
        CanalModel.RowData.Builder builder = CanalModel.RowData.newBuilder();
        builder.setLogfileName(this.logfilename);
        builder.setLogfileOffset(this.logfileoffset);
        builder.setExecuteTime(this.executeTime);
        builder.setTableName(this.tableName);
        builder.setEventType(this.eventType);
        for (String key : this.columns.keySet()) {
            builder.putColumns(key, this.columns.get(key));
        }
        return builder.build().toByteArray();
    }



    @Override
    public String toString() {
        return JSON.toJSONString(this);
        //return this.logfilename;
    }


}
