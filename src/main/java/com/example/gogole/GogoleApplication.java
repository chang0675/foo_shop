package com.example.gogole;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GogoleApplication {

    public static void main(String[] args) {
        SpringApplication.run(GogoleApplication.class, args);
    }

}
