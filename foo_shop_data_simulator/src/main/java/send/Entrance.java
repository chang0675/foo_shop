package send;


import jdk.nashorn.internal.objects.annotations.Where;
import org.junit.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 数据模拟器
 */
@Component
@EnableScheduling
public class Entrance {

    //生成一条数据
    @Test
    public void sendSingleSim() throws SQLException, InterruptedException {
        // 1. 生成点击流访问日志
        ClickLogSimulator clickLogSimulator = new ClickLogSimulator();
        //生成一条数据
        clickLogSimulator.sendToKafka();
        // 2. 生成添加到购物车消息
        CartData cartData = new CartData();
        //生成一条数据
        cartData.sendToKafka();
        // 3. 商品消息
        GoodsData goodsData = new GoodsData();
        goodsData.sendToKafka();
        // 4. 买家评价消息
        CommentsData commentsData = new CommentsData();
        //生成一条数据
        commentsData.sendToKafka();
//        //5. 订单明细数据
        String startDate = "2020-03-01 00:00:00";
        String finishDate = "2020-03-30 00:00:00";
        OrderData orderData = new OrderData(startDate, finishDate);
        orderData.sendTomysql();
    }

    //批量生成数据
    @Test
    @Scheduled(cron = "0/3 * * * * ?")
    public void sendBatchClickLogSim() throws SQLException, InterruptedException {

        while (true) {
            // 1. 生成点击流访问日志
            ClickLogSimulator clickLogSimulator = new ClickLogSimulator();
            //批量生成数据
            clickLogSimulator.sendToKafka();//ods_itcast_click_log

            Thread.sleep(1000);
        }



    }

    //    //模拟生成购物车数据
    @Test
//    @Scheduled(cron = "00/1 * * * * ?")
    public void sendBatchCartSim() throws SQLException {
        while (true) {
            // 2. 生成添加到购物车消息
            CartData cartData = new CartData();
            //批量生成数据
            cartData.sendToKafka();//ods_itcast_cart
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    //模拟生成商品消息
    @Test
    @Scheduled(cron = "00/1 * * * * ?")
    public void sendBatchGoodsSim() throws SQLException {
        // 3. 商品消息
        GoodsData goodsData = new GoodsData();
        goodsData.sendTomysql();//itcast_goods
    }

    //模拟生成买家评价消息
    @Test
    @Scheduled(cron = "0/3 * * * * ?")
    public void sendBatchCommentsSim() throws SQLException {
        // 4. 买家评价消息
        CommentsData commentsData = new CommentsData();
        //批量生成数据
        commentsData.sendToKafka();//ods_itcast_comments
    }

//    生成订单数据
    @Test
    @Scheduled(cron = "0/1 * * * * ?")
    public void sendOrderData() throws InterruptedException, SQLException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");

        Date d = new Date();
        long time = d.getTime()-13*24*60*60*1000;
        Date d2 = new Date(time);
        String s1 = format1.format(d2);

        while ( true) {
            Date date = new Date();
            String s = format.format(date);

            String startDate = s1+" 00:00:00";
            String finishDate = s;

            OrderData orderData = new OrderData(startDate, finishDate);
            orderData.sendTomysql();
            Thread.sleep(3000);
        }

    }

    @Test
    public void test01(){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String s = format.format(date);

        String finishDate = s;
        System.out.println(s);

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");

        Date d = new Date();
        long time = d.getTime()-13*24*60*60*1000;
        Date d2 = new Date(time);
        String s1 = format1.format(d2);

        System.out.println(s1);
    }


}
