package send;

import javafx.util.Pair;
import utils.DBSimulator;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 商品数据模拟器
 */
public class GoodsData extends DBSimulator {


    public void sendTomysql() throws SQLException {
        insertGoodsInfo();
    }

    //获取商品信息
    public void insertGoodsInfo() throws SQLException {
        Pair<Integer, String> userInfo = null;
        String sql = "INSERT INTO foo_goods(`goodsSn`,`productNo`,`goodsName`,`goodsImg`,`shopId`,`goodsType`,`marketPrice`,`shopPrice`," +
                        " `warnStock`,`goodsStock`,`goodsUnit`,`goodsTips`,`isSale`,`isBest`,`isHot`,`isNew`,`isRecom`," +
                        " `goodsCatIdPath`,`goodsCatId`,`shopCatId1`,`shopCatId2`,`brandId`,`goodsDesc`,`goodsStatus`," +
                        " `saleNum`,`saleTime`,`visitNum`,`appraiseNum`,`isSpec`,`gallery`,`goodsSeoKeywords`,`illegalRemarks`," +
                        "        `dataFlag`,`createTime`,`isFreeShipping`,`goodsSerachKeywords`,`modifyTime`)" +
                        " SELECT `goodsSn`,`productNo`,`goodsName`,`goodsImg`,`shopId`,`goodsType`,`marketPrice`,`shopPrice`," +
                        " `warnStock`,`goodsStock`,`goodsUnit`,`goodsTips`,`isSale`,`isBest`,`isHot`,`isNew`,`isRecom`," +
                        " `goodsCatIdPath`,`goodsCatId`,`shopCatId1`,`shopCatId2`,`brandId`,`goodsDesc`,`goodsStatus`," +
                        " `saleNum`,`saleTime`,`visitNum`,`appraiseNum`,`isSpec`,`gallery`,`goodsSeoKeywords`,`illegalRemarks`," +
                        "        `dataFlag`,`createTime`,`isFreeShipping`,`goodsSerachKeywords`,`modifyTime`" +
                        " FROM foo_goods ORDER BY RAND() LIMIT 1;";
        try {
            conn.setAutoCommit(false);//JDBC中默认是true，自动提交事务
            ps = conn.prepareStatement(sql);
            ps.execute();

            // 获取插入的订单Id
            String sqlLastInsertId = "SELECT LAST_INSERT_ID()";
            Statement statement = conn.createStatement();
            rs = statement.executeQuery(sqlLastInsertId);
            while(rs.next()) {
                int goodId = rs.getInt(1);
                ThreadLocalRandom random = ThreadLocalRandom.current();

                //修改商品上架/下架状态及上架时间
                sql = "update foo_goods set saleTime=?, isSale=?,createTime=? where goodsId=?";
                ps = (PreparedStatement) conn.prepareStatement(sql);
                String currDateTime = timeToDateString((new Date().getTime()), "yyyy-MM-dd HH:mm:ss");
                ps.setString(1, currDateTime);
                ps.setInt(2, random.nextInt(2));
                ps.setString(3, currDateTime);
                ps.setLong(4, goodId);
                ps.execute();

                System.out.println("发送商品日志消息>>>当前订单创建日期 ： " + currDateTime);
            }
            conn.commit();//提交事务
            rs.close();
            ps.close();
        }  catch (SQLException e) {
            conn.rollback();//回滚，出现异常后两条数据都执行不成功
            e.printStackTrace();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static String timeToDateString(Long dateTime,String format){
        Date date = new Date(dateTime);

        SimpleDateFormat sdf= null;
        String dateString = null;
        sdf= new SimpleDateFormat(format);
        dateString = sdf.format(date);
        return dateString;
    }


    @Override
    public void sendToKafka() throws SQLException, InterruptedException {

    }
}
