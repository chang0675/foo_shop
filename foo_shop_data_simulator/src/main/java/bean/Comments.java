package bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  评论数据
 * */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Comments {
    private String userId;              // 用户ID
    private String userName;            // 用户名
    private String orderGoodsId;        // 订单明细ID
    private Integer starScore;          // 评分
    private String comments;            // 评论内容
    private Long goodsId;               // 商品id
    private String imageViedoJSON;      // 图片、视频JSON
    private Long timestamp;             // 评论时间


}