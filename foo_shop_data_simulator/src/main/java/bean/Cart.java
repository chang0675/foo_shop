package bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *   购物车实体类对象
 * */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cart {
    private String goodsId;     // 商品ID
    private String userId;      // 用户ID
    private Integer count;      // 商品数量
    private String guid;        // 客户端唯一识别号
    private Long addTime;       // 添加到购物车时间
    private String ip;          // ip地址
}
