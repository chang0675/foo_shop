package bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *   商品宽表
 *
 * */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsWideBean {
    private String goodsId;         //商品id
    private String goodsSn;         //商品编号
    private String productNo;       //商品货号
    private String goodsName;       //商品名称
    private String goodsImg;        //商品图片
    private String shopId;          //门店ID
    private String goodsType;       //货物类型
    private String marketPrice;     //市场价
    private String shopPrice;       //门店价
    private String warnStock;       //预警库存
    private String goodsStock;      //商品总库存
    private String goodsUnit;       //单位
    private String goodsTips;       //促销信息
    private String isSale;          //是否上架	0:不上架 1:上架
    private String isBest;          //是否精品	0:否 1:是
    private String isHot;           //是否热销产品	0:否 1:是
    private String isNew;           //是否新品	0:否 1:是
    private String isRecom;         //是否推荐	0:否 1:是
    private String goodsCatIdPath;  //商品分类ID路径	catId1_catId2_catId3
    private String goodsCatId;      //goodsCatId	最后一级商品分类ID
    private String shopCatId1;          //门店商品分类第一级ID
    private String shopCatId2;          //门店商品第二级分类ID
    private String brandId;         //品牌ID
    private String goodsDesc;       //商品描述
    private String goodsStatus;     //商品状态	-1:违规 0:未审核 1:已审核
    private String saleNum;         //总销售量
    private String saleTime;        //上架时间
    private String visitNum;        //访问数
    private String appraiseNum;     //评价书
    private String isSpec;          //是否有规格	0:没有 1:有
    private String gallery;         //商品相册
    private String goodsSeoKeywords;  //商品SEO关键字
    private String illegalRemarks;  //状态说明	一般用于说明拒绝原因
    private String dataFlag;        //	删除标志	-1:删除 1:有效
    private String createTime;
    private String isFreeShipping;
    private String goodsSerachKeywords;
    private String modifyTime;      //修改时间
}