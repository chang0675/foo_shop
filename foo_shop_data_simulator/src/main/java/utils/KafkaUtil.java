package utils;

import org.apache.kafka.clients.producer.KafkaProducer;

import java.util.Properties;

//kafka工具类
public class KafkaUtil {
    //静态变量
    public static KafkaProducer producer;
    //静态代码块
    static {
        Properties kafkaProps = new Properties();
        kafkaProps.put("bootstrap.servers", ConfigReader.kafka_bootstrap_servers);
        kafkaProps.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("acks", "all");
        kafkaProps.put("retries", 5);
        producer = new KafkaProducer<String, String>(kafkaProps);
    }

}
