package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
/**
 * 读取配置文件
 */
public class ConfigReader {
    //静态变量
    private static  Properties properties;
    //通过类加载器获取配置文件的信息
    static {
            properties=new Properties();
            InputStream inputStream = ConfigReader.class.getClassLoader().getResourceAsStream("application.properties");
            try {
                properties.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    //获取信息
    public static String mysql_driver = properties.getProperty("mysql.driver");
    public static String mysql_server = properties.getProperty("mysql.server");
    public static String mysql_port = properties.getProperty("mysql.port");
    public static String mysql_database = properties.getProperty("mysql.database");
    public static String mysql_username = properties.getProperty("mysql.username");
    public static String mysql_password = properties.getProperty("mysql.password");
    public static  String kafka_bootstrap_servers = properties.getProperty("kafka.bootstrap.servers");
    public static  String output_topic_cart = properties.getProperty("input.topic.cart");
    public static  String output_topic_clicklog = properties.getProperty("input.topic.click_log");
    public static  String output_topic_comments = properties.getProperty("input.topic.comments");
    //测试
    public static void main(String[] args) {
        System.out.println(mysql_driver);
        System.out.println(mysql_server);
        System.out.println(mysql_port);
        System.out.println(mysql_database);
        System.out.println(mysql_username);
        System.out.println(mysql_password);
        System.out.println(output_topic_cart);
    }


}
