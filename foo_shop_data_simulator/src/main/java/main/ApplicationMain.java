package main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import send.Entrance;

import java.sql.SQLException;



@SpringBootApplication
public class ApplicationMain{
    public static void main(String[] args) throws SQLException {
       SpringApplication.run(ApplicationMain.class,args);
    }
}
